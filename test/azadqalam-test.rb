require 'httparty' 


class AzadqalamTest 

    def initialize(url="https://api.azadqalam.ir")
        @url = url 
    end 

    def test(font="Vazir")
        request = HTTParty.get("#{@url}/fonts?font=#{font}") 
        # Parameter should be taken from the command line. 

        return request.body 
    end 
end 