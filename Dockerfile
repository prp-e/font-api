FROM ruby:2.6

RUN mkdir -pv /usr/src/app
RUN echo "Folders made successfully" 

ADD . /usr/src/app 
WORKDIR /usr/src/app 

CMD ["bundle", "install"]
CMD ["ruby", "/usr/src/app/api.rb"]